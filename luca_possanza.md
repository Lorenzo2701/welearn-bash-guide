Comandi bash, su file:

1) cat

It can be used for the following purposes under UNIX or Linux.

Display text files on screen
Copy text files
Combine text files
Create new text files

TRADUZIONE:
Può essere utilizzato per i seguenti scopi in UNIX o Linux.

Visualizza file di testo sullo schermo
Copia file di testo
Combina file di testo
Crea nuovi file di testo

Example:
cat filename
cat file1 file2 
cat file1 file2 > newcombinedfile
cat < file1 > file2 #copy file1 to file2

2) diff

Compares files, and lists their differences.

Example:
diff filename1 filename2

TRADUZIONE:
Confronta i file ed elenca le loro differenze.
diff nomefile1 nomefile2


3) find

Find files in directory
find directory options pattern

TRADUZIONE:
Trova i file nella directory
trova il modello delle opzioni di directory

Example:
$ find . -name README.md
$ find /home/user1 -name '*.png'